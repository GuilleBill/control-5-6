#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>


float desvStd(estudiante curso[]){
	float varianza = 0.0, suma = 0.0, promedio = 0.0, valRestado, desvEstandar;//se declaran las variables q se ocuparan 
	for (int i = 0; i < 24; i++){
		suma = curso[i].prom + suma; 	//se suma todos los promedios de los alumnos		
	}
	promedio = (suma)/24; //luego se divide por la cantidad de alumnos para asi sacar el promedio de los alumnos 
	for (int i = 0; i < 24; i++){
		valRestado = curso[i].prom - promedio; //se saca la diferencia de los promedios de los alumnos y el promedio general
		varianza = pow(valRestado, 2) + varianza; //para obtener la varianza se necesita la diferencia de todos los promedios de los alumnos con el promedio general elevado a 2 y se suma cada diferencia
	}
	desvEstandar = sqrt(varianza/23); //para sacar la desviacion estandar es la varianza devidido en menos un elemento, en este caso al ser 24 alumnos se resta uno y queda 23 dividiendo asi la varianza por 23
	return desvEstandar;	
}

float menor(estudiante curso[]){
	float min;
	min = curso[0].prom; //se hace la suposicion inicial que el primer valor (promedio alumnos) es el numero menor 
	for(int i = 0; i <24 ; i++){
		if (min > curso[i].prom){ //esta condicion es para ir revisando si hay algun promedio inferior
			min = curso[i].prom;//si lo hay este se guarda en la variable min
		}
	}
	return min;
}

float mayor(estudiante curso[]){
	float max;
	max = curso[0].prom;
	for(int i = 0; i <24 ; i++){ //se hace la suposicion inicial que el primer valor (promedio alumnos) es el numero mayor 
		if (max < curso[i].prom){ //esta condicion es para ir revisando si hay algun promedio superior
			max = curso[i].prom;//si lo hay este se guarda en la variable max
		}
	}
	return max;
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones 
	for (int i = 0; i < 24; i++){ //se recorre el arreglo completo para asi asignarle valor 
		
		printf("Ingrese la nota del proyecto 1: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.proy1); //se almacena el valor
		printf("Ingrese la nota del proyecto 2: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.proy2);//se almacena el valor
		printf("Ingrese la nota del proyecto 3: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.proy3);//se almacena el valor

		printf("Ingrese la nota del control 1: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont1);//se almacena el valor
		printf("Ingrese la nota del control 2: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont2);//se almacena el valor
		printf("Ingrese la nota del control 3: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont3);//se almacena el valor
		printf("Ingrese la nota del control 4: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont4);//se almacena el valor
		printf("Ingrese la nota del control 5: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont5);//se almacena el valor
		printf("Ingrese la nota del control 6: ");//se pide ingresar la notas 
		scanf("%f", &curso[i].asig_1.cont6);//se almacena el valor
		
		curso[i].prom = (curso[i].asig_1.proy1*0.2) + (curso[i].asig_1.proy2*0.2) + (curso[i].asig_1.proy3*0.3) + (curso[i].asig_1.cont1*0.05) + (curso[i].asig_1.cont2*0.05) + (curso[i].asig_1.cont3*0.05) + (curso[i].asig_1.cont4*0.05) + (curso[i].asig_1.cont5*0.05) + (curso[i].asig_1.cont6*0.05);
//en la linea de arriba es para sacar el promedio de cada estudiante, con el porcentaje correspondiente de cada evaluacion 
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	int i;
	FILE *Aprobado_file; //se crea el archivo Aprobado_file
	FILE *Reprobado_file;//se crea el archivo Reprobado_file
	Reprobado_file = fopen("Reprobados.txt", "w"); //se abre el archivo en modo escritura 
	Aprobado_file = fopen("Aprobados.txt", "w");//se abre el archivo en modo escritura 
	if(((Aprobado_file=fopen(path,"w"))==NULL)&&((Reprobado_file=fopen(path,"w"))==NULL)){ //se comprueba q los archivos no esten nulos y si ambos lo estan lo dice en pantalla y luego se cierra
		printf("\nerror al crear el archivo");
		exit(0); 
	}else{
		for (int i = 0; i<24; i++){
			if(curso[i].prom < 4.0){//recorre el archivo buscando los promedios de los alumnos, si estos son menor a 4.0 estos se registran en el archivo llamado Reprobados 
				fprintf(Reprobado_file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
 curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);//se guarda toda la informacion del estudiante 
		}
			else{// si el promedio es mayor o igual a 4.0 este se guarda en el archivo Aprobado
				fprintf(Aprobado_file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
 curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);//se guarda toda la informacion del estudiante 
			}
		}
		fclose(Reprobado_file); //se cierra el archivo Reprobado_file
		fclose(Aprobado_file);//se cierra el archivo Aprobado_file
	}
}


void metricasEstudiantes(estudiante curso[]){
	float desviacion, minimo, maximo;
	
	minimo = menor(curso); //llama a la funcion menor y se almacena en la variable minimo 
	printf("El menor promedio %.2f \n",minimo); //muestra en pantalla el valor de la variable minimo 

	maximo = mayor(curso); //llama a la funcion mayor y se almacena en la variable maximo 
	printf("El mayor promedio %.2f \n",maximo);//muestra en pantalla el valor de la variable maximo 

	desviacion = desvStd(curso); //llama a la funcion desStd y se almacena en la variable promedioo 
	printf("desviacion estandar del curso %.2f\n ", desviacion);//muestra en pantalla el valor de la variable desviacion
}



void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("Aprobados.txt", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}